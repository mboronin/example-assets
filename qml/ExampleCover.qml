/****************************************************************************
**
** Copyright (C) 2017 Open Mobile Platform Ltd.
** Contact: Kirill Chuvilin <k.chuvilin@omprussia.ru>
**
** This file is part of example-assets project.
** See https://gitlab.com/sailfishos-examples for examples.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.5
import Sailfish.Silica 1.0


CoverBackground {
    property string applicationName: Qt.application.name

    Image {
        anchors.fill: parent
        source: Qt.resolvedUrl("background.png")
        fillMode: Image.PreserveAspectCrop
        verticalAlignment: Image.AlignBottom
    }

    ColoredImage {
        id: sdkLogo
        anchors {
            left: parent.left
            leftMargin: Theme.horizontalPageMargin
            right: parent.right
            rightMargin: Theme.horizontalPageMargin
            top: parent.top
            topMargin: parent.height / 4 - height / 2
        }
        width: height
        height: parent.height / 3
        source: Qt.resolvedUrl("sdk-logo-a.svg")
        sourceSize.height: height
        sourceSize.width: width
        fillMode: Image.PreserveAspectFit
        color: Theme.primaryColor
    }

    ColoredImage {
        anchors.fill: sdkLogo
        source: Qt.resolvedUrl("sdk-logo-b.svg")
        sourceSize.height: parent.height
        sourceSize.width: parent.width
        fillMode: Image.PreserveAspectFit
        color: Theme.highlightColor
    }

    Label {
        text: applicationName
        anchors {
            left: parent.left
            leftMargin: Theme.paddingSmall
            right: parent.right
            rightMargin: Theme.paddingSmall
            top: sdkLogo.bottom
            bottom: parent.bottom
            bottomMargin: Theme.iconSizeMedium
        }
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.RichText
        font.bold: true
        wrapMode: Text.WordWrap
    }

    CoverActionList {
        id: actionList

        iconBackground: false

        CoverAction {
            iconSource: Qt.resolvedUrl("git-logo.svg")
            onTriggered: Qt.openUrlExternally("https://gitlab.com/sailfishos-examples/"
                                              + Qt.application.name)
        }
    }
}
