/****************************************************************************
**
** Copyright (C) 2017 Open Mobile Platform Ltd.
** Contact: Kirill Chuvilin <k.chuvilin@omprussia.ru>
**
** This file is part of example-assets project.
** See https://gitlab.com/sailfishos-examples for examples.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

import QtQuick 2.5
import Sailfish.Silica 1.0


ListItem {
    id: root

    property alias text: textLabel.text
    property alias icon: iconImage
    property alias description: descriptionLabel

    property real _topPadding: 0
    property real _bottomPadding: Theme.paddingSmall
    property real _leftPadding: Theme.horizontalPageMargin
    property real _righPadding: Theme.horizontalPageMargin
    property real _minimumContentHeight: Theme.itemSizeMedium - _topPadding - _bottomPadding

    contentHeight: _topPadding +
                   Math.max(_minimumContentHeight, iconImage.height, textColumn.height) +
                   _bottomPadding

    ColoredImage {
        id: iconImage
        anchors {
            top: parent.top
            topMargin: root._topPadding + Math.max((root._minimumContentHeight - height) / 2, 0)
            left: parent.left
            leftMargin: root._leftPadding
        }
        width: Theme.iconSizeMedium
        height: Theme.iconSizeMedium
        sourceSize.width: width
        sourceSize.height: height
        fillMode: Image.PreserveAspectFit
        color: root.highlighted ? Theme.highlightColor : "#00000000"
    }

    Column {
        id: textColumn
        anchors {
            top: parent.top
            topMargin: root._topPadding + Math.max((root._minimumContentHeight - height) / 2, 0)
            left: iconImage.right
            leftMargin: Theme.paddingMedium
            right: parent.right
            rightMargin: root._rightPadding
        }

        Label {
            id: textLabel
            width: parent.width
            color: root.highlighted ? Theme.highlightColor : Theme.primaryColor
            elide: Text.ElideMiddle
        }

        Label {
            id: descriptionLabel
            visible: text !== ""
            width: parent.width
            color: root.highlighted ? Theme.highlightColor : Theme.secondaryColor
            font.pixelSize: Theme.fontSizeSmall
        }
    }
}
