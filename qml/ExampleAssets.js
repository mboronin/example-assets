/****************************************************************************
**
** Copyright (C) 2017 Open Mobile Platform Ltd.
** Contact: Kirill Chuvilin <k.chuvilin@omprussia.ru>
**
** This file is part of example-assets project.
** See https://gitlab.com/sailfishos-examples for examples.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Open Mobile Platform Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

.pragma library


/**
 * Get a single-word type using MIME type of a file
 * according to the Sailfish icons with pattenr icon-m-file-*
 * @param {string} mimeType - the MIME type
 * @returns {string} apk, audio, document, formatted, image, note, other, pdf,
 *                   presentation, rpm, spreadsheet, vcard, video
 */
function fileType(mimeType) {
    switch (mimeType) {
    case "application/vnd.android.package-archive":
        return "apk";
    case "application/msword":
    case "application/rtf":
    case "application/vnd.oasis.opendocument.text":
    case "application/vnd.oasis.opendocument.text-master":
    case "application/vnd.oasis.opendocument.text-template":
    case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
    case "application/vnd.openxmlformats-officedocument.wordprocessingml.template":
    case "application/vnd.ms-works":
    case "application/x-mswrite":
        return "document";
    case "application/javascript":
    case "application/json":
    case "application/xml":
    case "text/cmd":
    case "text/css":
    case "text/html":
    case "text/php":
    case "text/xml":
    case "text/markdown":
        return "formatted";
    case "text/plain":
    case "text/x-vnote":
        return "note";
    case "application/pdf":
        return "pdf";
    case "application/vnd.ms-powerpoint":
    case "application/vnd.oasis.opendocument.presentation":
    case "application/vnd.oasis.opendocument.presentation-template":
    case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
    case "application/vnd.openxmlformats-officedocument.presentationml.template":
    case "application/x-kpresenter":
        return "presentation";
    case "application/x-rpm":
        return "rpm";
    case "application/vnd.ms-excel":
    case "application/vnd.oasis.opendocument.spreadsheet":
    case "application/vnd.oasis.opendocument.spreadsheet-template":
    case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
    case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":
    case "application/x-kspread":
    case "text/csv":
        return "spreadsheet";
    case "text/vcard":
        return "vcard";
    default:
        if (mimeType.indexOf("audio/") === 0)
            return "audio";
        if (mimeType.indexOf("image/") === 0)
            return "image";
        if (mimeType.indexOf("video/") === 0)
            return "video";
        return "other";
    }
}


/**
 * Get system icon for the file type
 * @param {string} type - the MIME type or single-word file type
 * @returns {string} url of the system icon
 */
function fileIcon(type) {
    if (type.indexOf("/") >= 0) // if the MIME type is given
        type = fileType(type); // cast to a single-word type
    return "image://theme/icon-m-file-" + type;
}
