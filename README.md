# Example Assets
A set of common Qt elements for
[Sailfish OS example applications](https://gitlab.com/sailfishos-examples).

Copyright &copy; [Open Mobile Platform Ltd](http://omprussia.ru).  
License: [BSD-3-Clause](LICENSE.BSD-3-Clause.md).  
Contact: Kirill Chuvilin <<k.chuvilin@omprussia.ru>>

## Include to project
The assets are distributed as a Qt project available for inclusion (pri).
Follow the next steps to use it:

1. Copy the project to the root directory (that contains pro-file) of the Qt project.
  If you use git to manage your source code you can add this project as a submodule:  
  `git submodule add https://gitlab.com/sailfishos-examples/example-assets.git`.
2. Include the pri-file of this project to the pro-file of your project:  
  `include($$PWD/example-assets/example-assets.pri)`.

## Usage
To use assets in your QML files import the corresponfing module:  
`import ExampleAssets 1.0`.

The following QML-types are available:

* [`ColoredImage`](qml/ColoredImage.qml) is the `Image` that can be colored with the `color` property.
* [`ExampleCover`](qml/ExampleCover.qml) is the standard example cover implementation.
* [`IconTextListItem`](qml/IconTextListItem.qml) is the `ListItem` with icon and text.
